{
	"root": true,
	"ignorePatterns": ["dist", "node_modules", "src/env.d.ts", "functions", "coverage"],

	"extends": [
		"strictest/eslint",
		"strictest/promise",
		"strictest/typescript-eslint",
		"strictest/unicorn",
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"plugin:prettier/recommended",
		"plugin:astro/recommended",
		"plugin:jsx-a11y/recommended",
	],
	"plugins": [
		"file-progress",
		"deprecation",
		"prettier",
		"unicorn",
		"promise",
		"import",
		"jsx-a11y",
		"@typescript-eslint",
	],
	"parser": "@typescript-eslint/parser",
	"parserOptions": {
		"project": "./tsconfig.json",
		"tsconfigRootDir": "./",
	},
	"rules": {
		"file-progress/activate": 1,
		"deprecation/deprecation": "warn",
		"prettier/prettier": "warn",
		"no-constant-condition": "warn",
		"no-console": ["warn", { "allow": ["error", "warn"] }], // Sometimes we need to log errors on the back-end
		"no-dupe-else-if": "warn",
		"consistent-return": "off", // TS handles this
		"no-duplicate-imports": "off", // eslint-plugin-imports handles this
		"import/no-duplicates": "warn",
		"import/no-default-export": "error",
		"import/extensions": ["error", "ignorePackages"],
		"@typescript-eslint/no-empty-interface": "off",
		"@typescript-eslint/require-await": "warn",
		"@typescript-eslint/no-inferrable-types": "off",
		"@typescript-eslint/no-unused-vars": "warn",
		"@typescript-eslint/explicit-member-accessibility": [
			"error",
			{ "accessibility": "no-public", "overrides": { "properties": "off" } },
		],
		"@typescript-eslint/explicit-function-return-type": [
			"error",
			{ "allowConciseArrowFunctionExpressionsStartingWithVoid": true },
		],
		"@typescript-eslint/consistent-type-definitions": ["warn", "interface"],
		"@typescript-eslint/array-type": ["warn", { "default": "generic" }],
		"@typescript-eslint/dot-notation": "off",
		"@typescript-eslint/consistent-type-imports": "warn",
		"unicorn/catch-error-name": ["warn", { "name": "error" }],
	},
	"overrides": [
		{
			"files": ["*.astro"],
			"parser": "astro-eslint-parser",
			"parserOptions": {
				"parser": "@typescript-eslint/parser",
				"extraFileExtensions": [".astro"],
			},
			"rules": {
				"no-undef": "off", // Typescript handles this
				"@typescript-eslint/no-unused-vars": "off", // ESLint doesn't understand Astro's `Props` magic
				"unicorn/filename-case": "off", // File-based routing may require hyphenated filenames
			},
		},
		{
			"files": ["*.test.ts"],
			"plugins": ["vitest"],
			"extends": ["plugin:vitest/recommended"],
		},
		{
			"files": ["./test/**/*.ts"],
			"rules": {
				"func-names": "off", // Nightwatch tests use unnamed functions
				"unicorn/filename-case": "off", // Integration test stories sometimes have strange names
			},
		},
	],
}
