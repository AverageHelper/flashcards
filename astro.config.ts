/* eslint-disable import/no-default-export */

import { defineConfig } from "astro/config";
import cloudflare from "@astrojs/cloudflare";

// https://astro.build/config
export default defineConfig({
	site: "https://flashcards.average.name",
	root: ".", // Project root is the working directory
	outDir: "./dist", // Build to `/dist`
	publicDir: "./public", // Static assets live in `/public`
	srcDir: "./src", // Component sources live in `/src`
	output: "server", // Enable SSR
	adapter: cloudflare({
		mode: "directory", // Use a Cloudflare Pages function to handle requests
		imageService: "passthrough", // Silence compiler warning about images by specifying the default image service
		runtime: {
			mode: "local",
			type: "pages",
			bindings: {
				D1: {
					type: "d1",
				},
			},
		},
	}),
	build: {
		format: "file", // Build HTML pages at root, not in subdirectories
		assets: "assets", // Call the build assets folder "assets" instead of "_astro"
		inlineStylesheets: "always", // Inline stylesheets so they load quickly
	},
	vite: {
		server: {
			watch: {
				// Don't rebuild when Nightwatch runs, or Astro chokes:
				ignored: ["**/tests_output/**/*"],
			},
		},
	},
	devToolbar: {
		enabled: false, // Don't show dev controls in the webpage
	},
	compressHTML: false, // Pretty output for nerds
});
