import type { Awaitable, NightwatchAPI, NightwatchAssertionsResult } from "nightwatch";
import { dataTest } from "./utils/dataTest.ts";

describe("Registration and Login", () => {
	beforeEach(browser => void browser.navigateTo(browser.baseUrl));

	const testEmail = "test@localhost";
	const testDisplayName = "foobar";
	const testPassword = "Pa$$w0rd1234";

	function tryRegistrationWithDisplayName(
		browser: NightwatchAPI,
		displayName: string,
	): Awaitable<NightwatchAPI, NightwatchAssertionsResult<boolean>> {
		return tryRegistration(browser, {
			email: testEmail,
			displayName,
			password: testPassword,
		});
	}

	/**** Login guard ****/ {
		test("Login fails with fake credentials", async browser => {
			await browser
				.click(dataTest("nav-login")) // navigate to login
				.assert.urlEquals(`${browser.baseUrl}/login`)
				.setValue("[name='username-or-email']", "nightwatch")
				.setValue("[name='password']", testPassword)
				.submitForm(dataTest("login-form"))
				.assert.visible(dataTest("incorrect-credentials"));
		});

		test("Login fails to log in to 'admin' user", async browser => {
			await browser
				.click(dataTest("nav-login")) // navigate to login
				.assert.urlEquals(`${browser.baseUrl}/login`)
				.setValue("[name='username-or-email']", "admin")
				.setValue("[name='password']", testPassword)
				.submitForm(dataTest("login-form"))
				.assert.visible(dataTest("incorrect-credentials"));
		});
	}

	/**** Registration validation ****/ {
		test("Can visit registration", async browser => {
			await browser
				.click(dataTest("nav-login")) // navigate to login
				.click(dataTest("register-from-form")) // navigate to register
				.assert.urlEquals(`${browser.baseUrl}/register`);
		});

		test("Cannot submit an empty form", async browser => {
			// Assumes the browser resets the password field on refresh, indicating a navigation was attempted and failed!
			await browser
				.click(dataTest("nav-login")) // navigate to login
				.click(dataTest("register-from-form")) // navigate to register
				.assert.urlEquals(`${browser.baseUrl}/register`)
				.setValue("[name='password']", "test")
				.assert.valueEquals("[name='password']", "test") // sanity check
				.submitForm(dataTest("register-form")) // force form to submit (might bypass HTML validation, idk)
				.assert.valueEquals("[name='password']", "")
				.assert.urlEquals(`${browser.baseUrl}/register`); // still on the register form
		});

		const disallowedUsernames = ["admin", "password", "username", "owner", "test"] as const;
		for (const displayName of disallowedUsernames) {
			test(`Cannot create a user named '${displayName}'`, async browser => {
				await tryRegistrationWithDisplayName(browser, displayName).assert.visible(
					dataTest("error-username-disallowed"), // username not allowed error
				);
			});
		}

		// Username labels for verified users (such as "admin") end in "a verified user",
		// so we should ensure users cannot create a username to get around that
		const disallowedSuffixes = ["verified_user", "_verified_user", "verifieduser", "_verifieduser"];
		for (const suffix of disallowedSuffixes) {
			test(`Cannot create a user that ends in '${suffix}'`, async browser => {
				const displayName = `${testDisplayName}${suffix}`;
				await tryRegistrationWithDisplayName(browser, displayName).assert.visible(
					dataTest("error-username-disallowed"), // username not allowed error
				);
			});
		}

		const badUsernames = ["a", "aa", "aa,", "aaa,", "foobar_", "foo-bar"] as const;
		for (const displayName of badUsernames) {
			test(`Cannot create a user named '${displayName}'`, async browser => {
				await tryRegistrationWithDisplayName(browser, displayName).assert.visible(
					dataTest("error-username-invalid"), // username validation error
				);
			});
		}

		// TODO: Fails to create a user with a duplicate email
		// TODO: Fails to create a user with a duplicate display name
		// TODO: Fails to create a user with duplicate email and display name
		// TODO: Fails to create a user with a weak password
		// TODO: Fails to create a user with too long of a password (rip argon2)
		// TODO: Succeeds in creating a user. (Need a way to use a clean database when testing)
	}

	afterEach(browser => void browser.end());
});

interface Credentials {
	email: string;
	displayName: string;
	password: string;
}

function tryRegistration(
	browser: NightwatchAPI,
	credentials: Credentials,
): Awaitable<NightwatchAPI, NightwatchAssertionsResult<boolean>> {
	return (
		browser
			.click(dataTest("nav-login")) // navigate to login
			.click(dataTest("register-from-form")) // navigate to register
			.assert.urlEquals(`${browser.baseUrl}/register`)
			.setValue("[name='email']", credentials.email)
			.setValue("[name='username']", credentials.displayName)
			.setValue("[name='password']", credentials.password)
			.setValue("[name='password-repeat']", credentials.password)
			// .setValue("[name='accepted-terms']", "on") // TODO: Enable this
			.submitForm(dataTest("register-form"))
			.assert.visible("[name='email']")
	); // Wait until after form submit, make sure we're still on the form page
}
