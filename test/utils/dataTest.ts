/**
 * @returns a selector for elements with the given `data-test` value.
 */
export function dataTest<ID extends string>(id: ID): `[data-test="${ID}"]` {
	return `[data-test="${id}"]`;
}
