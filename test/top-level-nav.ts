import { dataTest } from "./utils/dataTest.ts";

describe("Basic navigation flows", () => {
	const platformTitle = "Average Flashcards";
	const testDeck1 = "ade447c2-6677-4d70-8b86-83ecf897760a";
	const testDeck2 = "280a3804-40ac-46cb-94a4-52ee6299de9b";

	beforeEach(browser => void browser.navigateTo(browser.baseUrl));

	test("The title is correct", async browser => {
		await browser.assert.titleEquals(platformTitle);
	});

	test("Can visit decks", async browser => {
		await browser
			.click(dataTest("nav-decks"))
			.assert.titleEquals(`All Decks | ${platformTitle}`)
			.assert.urlEquals(`${browser.baseUrl}/decks`);
	});

	test("Can find our test decks", async browser => {
		await browser
			.click(dataTest("nav-decks"))
			.assert.urlEquals(`${browser.baseUrl}/decks`)
			.assert.visible(dataTest(`deck-item-${testDeck1}`))
			.assert.visible(dataTest(`deck-item-${testDeck2}`));
	});

	afterEach(browser => void browser.end());
});
