import { defineConfig } from "vitest/config";

export default defineConfig({
	test: {
		typecheck: {
			checker: "tsc",
			tsconfig: "./tsconfig.json",
		},
		environment: "miniflare",
		sequence: {
			hooks: "list",
		},
		coverage: {
			all: true,
			enabled: true,
			provider: "v8",
			reportsDirectory: "coverage",
			include: ["src"],
		},
	},
});
