# 2. Hash passwords with bcrypt for authentication

Date: 2024-03-02

## Status

Accepted

## Context

We need to guard write access by users. I don't want to depend too much on third parties for authentication, such as with email-based "magic links", for now. So, standard password auth. Goals are as follows:

- Follow [OWASP recommendations](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html), which means:
- Prefer Argon2id for the hash algorithm.
- Where Argon2id is unavailable, use scrypt.
- Barring scrypt, use bcrypt, but limit the password length to 72 bytes.
- Alternatively, consider PBKDF2, with at least 600,000 iterations.

Cloudflare, our current host, has certain limitations:

- Argon2id is not viable, for various reasons. [Every](https://github.com/antelle/argon2-browser) [implementation](https://github.com/ranisalt/node-argon2) right now requires either native Node features, or WASM which I don't know how to use yet.
- While Cloudflare has native structures to provide PBKDF2, Cloudflare [appears to be limited to only 100,000 iterations](https://lord.technology/2024/02/21/hashing-passwords-on-cloudflare-workers.html).
- [Script](https://github.com/ricmoo/scrypt-js) is complicated.

Bcrypt seems to work on Cloudflare, at least locally, via [`bcrypt.js`](https://github.com/dcodeIO/bcrypt.js).

If it turns out that bcrypt actually does not work on Cloudflare's edge nodes, then we will begin using "magic links" instead.

## Decision

We will use [`bcrypt.js`](https://github.com/dcodeIO/bcrypt.js) to hash passwords for storage. We will use a work factor of at least 10, and limit the password to 72 bytes.

## Consequences

Input length will be limited to 72 bytes, which doesn't always equate to 72 characters. We will need to validate the input length at the server endpoint.

We also need to keep track of the algorithm used, in case we later decide to change hash algorithms.

Using bcrypt means we cannot reliably pepper the input.
