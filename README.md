# Average Flashcards

A simple flash card site, accessible at [flashcards.average.name](https://flashcards.average.name).

Built with [Astro](https://astro.build) and TypeScript.

## Goals

Features that I think are necessary for a minimum-viable app, in this priority order:

- [x] Automatic light/dark color modes
- [x] "Run" a selected flashcard deck by showing one card after another in random order
- [x] List existing decks
- [ ] Let users create public flashcard decks, with `name` and `description` for each
- [ ] Let users modify decks they made

General design goals:

- Have fun building it
- Avoid client-side javascript wherever possible
- Load colors inline in HTML, to avoid blinking during load times
- Colorblind accessibility
- Score **"A"** or better on [securityheaders.com](https://securityheaders.com/?q=https%3A%2F%2Fflashcards.average.name)

Potential future directions which should be considered in the design process:

- Private card decks
- Share private card decks with other logged-in users
- Translate interface strings and flash cards
- Proper [Subresource Integrity](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity) metadata
- A separate card view designed for visually-impaired people (unless I can figure out how to adapt the existing logic without introducing Javascript)

## License

<!-- TODO: Figure out licensing for user-generated content. -->
<!-- All user-generated content that was both created and displayed by the instance of this software hosted at https://flashcards.average.name is licensed under the Creative Commons BY-SA license. -->

This project's source code is licensed under the [GNU General Public License v3.0](LICENSE) or any later version of that license.

## Usage

To run the project locally, first make sure you have Node 20 or a newer LTS version.

Then, install the project dependencies with `npm ci` or `npm install`.

### Database

Create a Cloudflare D1 database:

```sh
npx wrangler login
npx wrangler d1 create flashcards
```

Copy the resulting information into a new `wrangler.toml` file at the root of the repository:

```toml
# wrangler.toml

[[d1_databases]]
binding = "D1" # Should match preview_database_id
database_name = "flashcards"
database_id = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
preview_database_id = "D1" # Add this line if it's not included already
```

### Auth

User authentication is disabled by default. To configure authentication, create a file at the root of the repository called `.dev.vars`, and populate it like so:

```sh
# Whether users may log in. Any value other than `true` prevents both log-in and registration.
IS_AUTH_ENABLED=true

# The way new users may register.
# - `"disabled"` - The default registration mode. Prohibits new users from joining.
# - `"invite-only"` - Users are required to provide a unique invitation code to join.
# - `"open"` - Any user may create a new account.
USER_REGISTRATION_MODE="open"
```

Set these values in the Cloudflare Dashbaord to use them in a production environment.

Note that using a value of `"invite-only"` requires knowledge of SQLite in order to create invitation codes. There is not yet an easy UI for this.

For example, to construct an invite code, gather the requesite details:

- `id` will be the value returned by the `uuidgen` command, something like `F7497A89-12CD-4C12-A9B5-EFEAA081069D`
- `created_at` will be the value returned by the `echo $(($(date +%s) * 1000))` command, something like `1709764336000`
- `created_by` will be the ID found with the `npx wrangler d1 execute flashcards --local --command="SELECT id FROM users"` command, which should be `a04d8725-ec15-4f11-9112-167b71b8d30a`
- `value` will be the value returned by the `openssl rand -base64 6` command. This will be the string that a user must enter at registration. (The form check is case insensitive.)
- `total_uses` will be the number of total uses for the invite code.

Then substitute those details into an `INSERT` command against the local database:

```sh
npx wrangler d1 execute flashcards --local --command="INSERT INTO registration_invites (id, created_at, created_by, value, total_uses) VALUES ( \
'F7497A89-12CD-4C12-A9B5-EFEAA081069D', \
1709764336000, \
'a04d8725-ec15-4f11-9112-167b71b8d30a', \
'b6Dw5ZQu', \
1 \
)"
```

REMEMBER: You MUST wrap _strings_ in single quotes (`'`), and leave _numbers_ as they are.

A similar command would do the same for a production environment.

Get the list of invite codes with `npx wrangler d1 execute flashcards --local --command="SELECT * FROM registration_invites"`.

You can give users a link that would prefill the invite code:

```
https://flashcards.average.name/register?invite=b6Dw5ZQu
```

<!-- TODO: Build an admin UI for configuring invitation codes. -->

### Local Development

To run the project locally, use `npm start`.

Before deploying to a production environment, remember to run `npm run db:init:prod` to fill in the basic tables in the production environment. You may run this command multiple times if you need, as all of its changes \*should be\* idempotent.

### Tests

To run unit tests, use `npm test`.

To run integration tests with Nightwatch, use `npm run e2e`.

## Contributing

This project lives primarily at [git.average.name](https://git.average.name/AverageHelper/flashcards). Issues or pull requests should be filed there. You may sign in or create an account directly, or use one of several OAuth 2.0 providers from GitHub, GitLab, and others.

We've documented major architecture decisions [using ADR](doc/architecture/decisions/0001-record-architecture-decisions.md). You can install `adr-tools` from [their repository](https://github.com/npryce/adr-tools).
