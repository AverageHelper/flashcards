// Refer to the online docs for more details:
// https://nightwatchjs.org/gettingstarted/configuration/
//

const javascriptEnabled = false; // Test that we work without JS

/**
 * @type {import("nightwatch").NightwatchOptions}
 */
// eslint-disable-next-line no-undef
module.exports = {
	base_url: "http://localhost:8788",

	// See https://nightwatchjs.org/guide/concepts/page-object-model.html
	page_objects_path: [],

	// See https://nightwatchjs.org/guide/extending-nightwatch/adding-custom-commands.html
	custom_commands_path: [],

	// See https://nightwatchjs.org/guide/extending-nightwatch/adding-custom-assertions.html
	custom_assertions_path: [],

	// See https://nightwatchjs.org/guide/extending-nightwatch/adding-plugins.html
	plugins: [],

	// See https://nightwatchjs.org/guide/concepts/test-globals.html
	globals_path: "",

	webdriver: {
		start_process: true,
		server_path: "",
	},

	test_workers: {
		enabled: true,
		workers: "auto",
	},

	// Don't muck up the output with errors in the middle. Show them at the end instead
	disable_error_log: true,

	screenshots: {
		enabled: false,
		path: "screens",
		on_failure: true,
	},

	test_settings: {
		default: {
			desiredCapabilities: {
				browserName: "firefox",
				javascriptEnabled,
				acceptInsecureCerts: false,
				"moz:firefoxOptions": {
					args: ["--headless"],
				},
			},
		},

		safari: {
			desiredCapabilities: {
				browserName: "safari",
				javascriptEnabled,
				acceptInsecureCerts: false,
			},
		},

		firefox: {
			desiredCapabilities: {
				browserName: "firefox",
				javascriptEnabled,
				acceptInsecureCerts: false,
				"moz:firefoxOptions": {
					args: ["--headless"],
				},
			},
		},

		chrome: {
			desiredCapabilities: {
				browserName: "chrome",
				javascriptEnabled,
				acceptInsecureCerts: false,
				"goog:chromeOptions": {
					// More info on Chromedriver: https://sites.google.com/a/chromium.org/chromedriver/
					w3c: true,
					args: ["headless"],
				},
			},
		},

		edge: {
			desiredCapabilities: {
				browserName: "MicrosoftEdge",
				javascriptEnabled,
				acceptInsecureCerts: false,
				"ms:edgeOptions": {
					w3c: true,
					// More info on EdgeDriver: https://docs.microsoft.com/en-us/microsoft-edge/webdriver-chromium/capabilities-edge-options
					args: ["headless"],
				},
			},
			// webdriver: {
			// 	start_process: true,
			// 	// Follow https://docs.microsoft.com/en-us/microsoft-edge/webdriver-chromium/?tabs=c-sharp#download-microsoft-edge-webdriver
			// 	// to download the Edge WebDriver and set the location of extracted `msedgedriver` below:
			// 	server_path: "",
			// },
		},
	},
};
