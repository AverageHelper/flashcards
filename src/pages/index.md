---
layout: "@/layouts/ContentLayout.astro"
---

## Dead-simple flashcard decks.

There are lots of flashcard projects out there. This one is for me to see how hard it'd be to create my own, for fun.
