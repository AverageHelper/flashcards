---
import Layout from "@/layouts/Layout.astro";
import { env } from "@/utils/environment.js";
import { getInviteCode, getUserWithUsernameOrEmail } from "@/utils/db/index.js";
import { validate } from "superstruct";
import {
	ADMIN_USER_ID,
	disallowedUsernames,
	email as emailStruct,
	invitationCode,
	isNotNullOrFalse,
	PASSWORD_MAX_LENGTH,
	PASSWORD_MIN_LENGTH,
	passwordPattern,
	password as passwordStruct,
	USERNAME_MAX_LENGTH,
	USERNAME_MIN_LENGTH,
	usernamePattern,
	username as usernameStruct,
} from "@/utils/structs.js";

const { isLoginEnabled, userRegistrationMode } = env(Astro);

// Redirect to home if login isn't enabled
if (!isLoginEnabled) {
	return Astro.redirect("/", 302);
}

// Redirect to login if registration isn't enabled
if (userRegistrationMode === "disabled") {
	return Astro.redirect("/login", 302);
}

// See https://docs.astro.build/en/recipes/build-forms/
// Remember: Do not trust user input.
// Expect anyone to hit this endpoint using `curl` and skip the HTML form validation entirely.

const validationErrors = {
	general: null as string | null,
	inviteCode: null as string | null,
	inviteCodeInvalid: false,
	usernameAlreadyExists: false,
	usernameDisallowed: false,
	emailAlreadyExists: false,
	username: null as string | null,
	email: null as string | null,
	password: null as string | null,
	passwordRepeat: null as string | null,
	acceptedTerms: null as string | null,
};

let previousInviteCode: string = Astro.url.searchParams.get("invite") ?? "";
let previousEmail: string = "";
let previousUsername: string = "";
let previousPassword: string = "";

if (Astro.request.method === "POST") {
	// Handle form submit (server-side!)
	try {
		const data = await Astro.request.formData();
		const rawUsername = data.get("username"); // input with name="username"
		const rawEmail = data.get("email"); // etc.
		const rawPassword = data.get("password");
		const passwordRepeat = data.get("password-repeat");
		const rawInviteCode = data.get("invite");
		const acceptedTerms = data.get("accepted-terms") ?? "on"; // TODO: Remove this default once we have terms and a policy to accept

		// Validate form values
		// Be sure to trim and lowercase the username string
		const [usernameError, username] = validate(rawUsername, usernameStruct(), { coerce: true });
		if (usernameError) {
			validationErrors.username = usernameError.message;
		} else if (
			disallowedUsernames.has(username) ||
			username.endsWith("verified_user") ||
			username.endsWith("verifieduser")
		) {
			// Check that the username is not specifically prohibited
			validationErrors.usernameDisallowed = true;
		} else {
			previousUsername = username; // Return the value to the form in case of error

			// Check that the username is unused
			const result = await getUserWithUsernameOrEmail(Astro, username);
			if (result instanceof Error) {
				validationErrors.username =
					"Database error while checking that username. Please try again!";
			} else if (result !== null) {
				// Username is taken!
				validationErrors.usernameAlreadyExists = true;
			}
		}

		// Be sure to trim the email string
		const [emailError, email] = validate(rawEmail, emailStruct(), { coerce: true });
		if (emailError) {
			validationErrors.email = emailError.message;
		} else {
			previousEmail = email; // Return the value to the form in case of error

			// Check that the email is unused
			const result = await getUserWithUsernameOrEmail(Astro, email);
			if (result instanceof Error) {
				validationErrors.email = "Database error while checking that email. Please try again!";
			} else if (result !== null) {
				// Email is taken!
				validationErrors.emailAlreadyExists = true;
			}
		}

		const [inviteError, code] = validate(rawInviteCode, invitationCode(), { coerce: true });
		if (inviteError) {
			validationErrors.inviteCode = inviteError.message;
		} else {
			previousInviteCode = code; // Return the value to the form in case of error

			// Check that the code is valid
			const result = await getInviteCode(Astro, code);
			if (result instanceof Error) {
				validationErrors.inviteCode =
					"Database error while checking that invitation code. Please try again!";
			} else if (
				!result || // Invite code doesn't exist
				result.currentUses >= result.totalUses || // Invite code's uses are expended
				result.createdBy !== ADMIN_USER_ID // Invite code was made by someone other than the Admin user
			) {
				validationErrors.inviteCodeInvalid = true;
			}
		}

		const [passwordError, password] = validate(rawPassword, passwordStruct());
		if (passwordError) {
			validationErrors.password = passwordError.message;
		} else {
			previousPassword = password; // Return the value to the form in case of error
		}

		if (password !== passwordRepeat) {
			validationErrors.passwordRepeat = "Passwords should match";
		}

		// HTML checkbox sends the value "on" by default if checked
		if (acceptedTerms !== "on") {
			validationErrors.acceptedTerms =
				"You must accept the Terms of Service and Privacy Policy in order to continue";
		}

		const hasErrors = Object.values(validationErrors).some(isNotNullOrFalse);
		if (!hasErrors) {
			// TODO: Set cookie, then continue to email verification.
		}
	} catch (error) {
		console.error(error);
		validationErrors.general = "There was a problem creating your account. Please try again!";
	}
}
---

<Layout>
	<main class="content">
		<h1>Register a new account</h1>
		<p
			>All fields are required. Already have an account? <a
				href="/login"
				data-test="log-in-from-form">Log in!</a
			></p
		>

		<form class="account" method="post" data-test="register-form">
			{validationErrors.general && <p class="error">{validationErrors.general}</p>}

			{
				userRegistrationMode === "invite-only" && (
					<>
						<p class="requirements">Registration is on an invite-only basis.</p>
						<label>
							<span class="name">Invite code</span>
							<input
								class="lowercase"
								type="text"
								name="invite"
								autocomplete="off"
								placeholder=" "
								autofocus={!previousInviteCode}
								required
								value={previousInviteCode}
							/>
						</label>
						{validationErrors.inviteCode && <p class="error">{validationErrors.inviteCode}</p>}
						{validationErrors.inviteCodeInvalid && <p class="error">That code is invalid</p>}
					</>
				)
			}

			<p class="requirements">We only send account-related emails, no spam.</p>
			<label>
				<span class="name">Email</span>
				<input
					type="email"
					name="email"
					autocomplete="email"
					placeholder=" "
					autofocus
					required
					value={previousEmail}
				/>
			</label>
			{validationErrors.email && <p class="error">{validationErrors.email}</p>}
			{
				validationErrors.emailAlreadyExists && (
					<p class="error">
						That email is taken. Did you mean to <a href="/login">log in</a>?
					</p>
				)
			}

			<p class="requirements"
				>Between {USERNAME_MIN_LENGTH} and {USERNAME_MAX_LENGTH} lowercase letters, numbers, and underscores
				(<code>_</code>). Must begin with a letter.</p
			>
			<label>
				<span class="name">Username</span>
				<input
					class="lowercase"
					type="text"
					name="username"
					autocomplete="username"
					placeholder=" "
					required
					minlength={USERNAME_MIN_LENGTH}
					maxlength={USERNAME_MAX_LENGTH}
					pattern={usernamePattern}
					value={previousUsername}
				/>
			</label>
			{
				validationErrors.username && (
					<p class="error" data-test="error-username-invalid">
						{validationErrors.username}
					</p>
				)
			}
			{
				validationErrors.usernameAlreadyExists && (
					<p class="error" data-test="error-username-not-unique">
						That username is taken. Did you mean to <a href="/login">log in</a>?
					</p>
				)
			}
			{
				validationErrors.usernameDisallowed && (
					<p class="error" data-test="error-username-disallowed">
						Please choose a different username
					</p>
				)
			}

			<p class="requirements"
				>Between {PASSWORD_MIN_LENGTH} and {PASSWORD_MAX_LENGTH} uppercase, lowercase, and special characters.</p
			>
			<label>
				<span class="name">Password</span>
				<input
					type="password"
					name="password"
					autocomplete="new-password"
					placeholder=" "
					required
					minlength={PASSWORD_MIN_LENGTH}
					maxlength={PASSWORD_MAX_LENGTH}
					pattern={passwordPattern}
					value={previousPassword}
				/>
			</label>
			{validationErrors.password && <p class="error">{validationErrors.password}</p>}

			<label>
				<span class="name">Repeat password</span>
				<input
					type="password"
					name="password-repeat"
					autocomplete="new-password"
					placeholder=" "
					required
					minlength={PASSWORD_MIN_LENGTH}
					maxlength={PASSWORD_MAX_LENGTH}
					pattern={passwordPattern}
				/>
			</label>
			{validationErrors.passwordRepeat && <p class="error">{validationErrors.passwordRepeat}</p>}

			<!-- <label>
				<input type="checkbox" name="accepted-terms" required />
				{/* Using target="_blank" here so links open in a new tab, so users don't lose form data */}
				By creating an account, you agree to abide by our <a href="/terms" target="_blank"
					>Terms of Service</a
				> and
				<a href="/privacy" target="_blank">Privacy Policy</a>.
			</label> -->

			<button type="submit">Create an account</button>
		</form>
	</main>
</Layout>
