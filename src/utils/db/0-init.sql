-- Creates tables and test data, overwriting that test data where it already exists

BEGIN TRANSACTION;

----------------------------------
-- Invite codes
CREATE TABLE IF NOT EXISTS registration_invites (
	id          TEXT NOT NULL PRIMARY KEY, -- v4 UUID string
	created_at  INTEGER NOT NULL, -- number of seconds since 1970-01-01 00:00:00 UTC
	created_by  TEXT NOT NULL, -- v4 UUID string
	value       TEXT NOT NULL UNIQUE, -- The string users should enter to actuate this code
	total_uses  INTEGER NOT NULL
);

-- Populate the registration_invites table manually.

----------------------------------
-- Users
CREATE TABLE IF NOT EXISTS users (
	id                   TEXT NOT NULL PRIMARY KEY, -- v4 UUID string
	invite_code          TEXT, -- v4 UUID string
	created_at           INTEGER NOT NULL, -- number of seconds since 1970-01-01 00:00:00 UTC
	display_name         TEXT NOT NULL UNIQUE, -- no two users should have the same display name
	email                TEXT NOT NULL UNIQUE, -- no two users should have the same email address
	is_email_verified    INTEGER NOT NULL, -- 0 or 1
	password_potatoes    TEXT NOT NULL, -- encoded hash+salt mix
	password_updated_at  INTEGER NOT NULL, -- number of seconds since 1970-01-01 00:00:00 UTC
	allows_login         INTEGER NOT NULL, -- 0 or 1
	ban_reason           TEXT,
	email_verification_last_sent  INTEGER, -- number of seconds since 1970-01-01 00:00:00 UTC, or NULL if never sent
	password_reset_email_last_sent  INTEGER, -- number of seconds since 1970-01-01 00:00:00 UTC, or NULL if never sent
	tos_last_accepted    INTEGER -- number of seconds since 1970-01-01 00:00:00 UTC, or NULL if never accepted
);

-- The Admin User, used for actions done in the name of the platform
INSERT INTO users (id, invite_code, created_at, display_name, email, is_email_verified, password_potatoes, password_updated_at, allows_login, ban_reason, email_verification_last_sent, password_reset_email_last_sent, tos_last_accepted) VALUES (
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	NULL,
	1709320903356,
	'admin',
	'noreply@mails.average.name',
	1,
	'inert',
	1709320903356,
	0,
	'The admin user should never be logged into directly.',
	NULL,
	NULL,
	NULL
) ON CONFLICT(id) DO UPDATE SET
	created_at=excluded.created_at,
	display_name=excluded.display_name,
	email=excluded.email,
	is_email_verified=excluded.is_email_verified,
	password_potatoes=excluded.password_potatoes,
	password_updated_at=excluded.password_updated_at,
	allows_login=excluded.allows_login,
	ban_reason=excluded.ban_reason,
	email_verification_last_sent=excluded.email_verification_last_sent,
	password_reset_email_last_sent=excluded.password_reset_email_last_sent,
	tos_last_accepted=excluded.tos_last_accepted;

----------------------------------
-- Decks
CREATE TABLE IF NOT EXISTS decks (
	id                TEXT NOT NULL PRIMARY KEY, -- v4 UUID string
	created_at        INTEGER NOT NULL, -- number of seconds since 1970-01-01 00:00:00 UTC
	last_modified_at  INTEGER NOT NULL, -- number of seconds since 1970-01-01 00:00:00 UTC
	created_by        TEXT NOT NULL, -- v4 UUID string
	title             TEXT NOT NULL,
	description       TEXT NOT NULL
);

-- demo-random deck
DELETE FROM decks WHERE id='ade447c2-6677-4d70-8b86-83ecf897760a'; -- also deletes the deck cards (foreign key constraint)
INSERT INTO decks (id, created_at, last_modified_at, created_by, title, description) VALUES (
	'ade447c2-6677-4d70-8b86-83ecf897760a',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Random Demo',
	'A sample deck that we use to test the extremes of styles and behavior on the site.'
);

-- demo-jokes deck
DELETE FROM decks WHERE id='280a3804-40ac-46cb-94a4-52ee6299de9b'; -- also deletes the deck cards (foreign key constraint)
INSERT INTO decks (id, created_at, last_modified_at, created_by, title, description) VALUES (
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Jokes Demo',
	'A sample deck that we use to test common interaction flows.'
);

----------------------------------
-- Cards
CREATE TABLE IF NOT EXISTS cards (
	id                TEXT NOT NULL PRIMARY KEY, -- v4 UUID string
	deck_id           TEXT NOT NULL, -- v4 UUID string
	created_at        INTEGER NOT NULL, -- number of seconds since 1970-01-01 00:00:00 UTC
	last_modified_at  INTEGER NOT NULL, -- number of seconds since 1970-01-01 00:00:00 UTC
	created_by        TEXT NOT NULL, -- v4 UUID string
	front             TEXT NOT NULL,
	back              TEXT NOT NULL,

	-- Enforce a many-to-one relationship between cards and decks, deleting the cards if the deck is deleted
	FOREIGN KEY (deck_id)
		REFERENCES decks (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE,

	-- Enforce a many-to-one relationship between cards and decks, deleting the cards if the deck is deleted
	FOREIGN KEY (deck_id)
		REFERENCES decks (id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

-- cards in deck demo-random
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'6a75c8ee-b05f-4bb5-846d-927e28d4e9fd',
	'ade447c2-6677-4d70-8b86-83ecf897760a',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Foo',
	'Bar'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'c2eea869-9893-45ad-b803-5063487dc279',
	'ade447c2-6677-4d70-8b86-83ecf897760a',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Non sodales neque sodales ut etiam sit amet. Nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Amet nisl suscipit adipiscing bibendum. Sit amet tellus cras adipiscing enim eu turpis egestas pretium. Sollicitudin aliquam ultrices sagittis orci a scelerisque purus. At augue eget arcu dictum varius duis at.',
	'Elementum eu facilisis sed odio. Sollicitudin ac orci phasellus egestas tellus. Turpis in eu mi bibendum neque. Cursus risus at ultrices mi tempus. Purus semper eget duis at tellus. Quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor. Pulvinar elementum integer enim neque. Molestie at elementum eu facilisis sed. Volutpat diam ut venenatis tellus in metus vulputate. Vitae suscipit tellus mauris a. Est sit amet facilisis magna etiam.'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'65463d72-3341-4922-aef0-9f2cc6b2c1bd',
	'ade447c2-6677-4d70-8b86-83ecf897760a',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
	'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
);

-- cards in deck demo-jokes, Generated from https://www.randomlists.com/jokes?dup=false&qty=10
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'bc74fc43-cdae-43ee-964c-59be2bf7878e',
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'What can be broken, but can''t be held?',
	'A promise.'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'3c51f8a5-83a6-42f7-89a7-8dc3a9dae234',
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'What do you call an alligator in a vest?',
	'An investigator!'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'089c7a24-6d69-41f6-9546-61602efed573',
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'What did the policeman say to his belly button?',
	'You''re under a vest.'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'745b460e-bb44-42f6-b7c4-5c8d682a3199',
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Why did the cookie go to the doctors?',
	'Because he felt crummy.'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'ff06136d-44fd-442d-be46-3e17f89c1082',
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Why did the computer go to the doctor?',
	'It had a virus.'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'cf5ec7a3-bdca-48ec-a35f-135754f7ba97',
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'Why couldn''t the pony sing?',
	'Because she was a little hoarse.'
);
INSERT INTO cards (id, deck_id, created_at, last_modified_at, created_by, front, back) VALUES (
	'752ffe1f-86a9-4f42-8e0c-46314293c6a4',
	'280a3804-40ac-46cb-94a4-52ee6299de9b',
	1709320903357,
	1709320903357,
	'a04d8725-ec15-4f11-9112-167b71b8d30a',
	'How many tickles does it take to get an octopus to laugh?',
	'Ten-tickles.'
);

COMMIT;
