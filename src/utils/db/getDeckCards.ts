import type { AstroGlobal } from "astro";
import type { CardData, CardDbData } from "../structs.js";
import { assert } from "superstruct";
import { cardDataFromDb, cardDbData } from "../structs.js";
import { errorFromUnknownError } from "../errorFromUnknownError.js";

function isCardData(tbd: unknown): tbd is CardDbData {
	try {
		assert(tbd, cardDbData);
		return true;
	} catch (error) {
		console.warn("Improper card found:", error, tbd);
		return false;
	}
}

/**
 * Returns a shuffled array of cards associated with the deck
 * with the given `deckId`.
 */
export async function getDeckCards(
	Astro: AstroGlobal,
	deckId: string | undefined,
): Promise<Result<Array<CardData>>> {
	// Ignore empty ID
	if (!deckId) return [];

	try {
		const cardsQuery = Astro.locals.runtime.env.D1.prepare(
			`SELECT * FROM cards
			WHERE deck_id = ?1
			ORDER BY RANDOM()`,
		).bind(deckId);
		const cardsResult = await cardsQuery.all();
		const cards = cardsResult.results.filter(isCardData).map(cardDataFromDb); // Ignore malformed data

		return cards;
	} catch (error) {
		console.error(error);
		return errorFromUnknownError(error);
	}
}
