import type { AstroGlobal } from "astro";
import type { UserData, UserDbData } from "../structs.js";
import { assert } from "superstruct";
import { userDbData, userDataFromDb } from "../structs.js";
import { errorFromUnknownError } from "../errorFromUnknownError.js";

function isUserData(tbd: unknown): tbd is UserDbData {
	try {
		assert(tbd, userDbData);
		return true;
	} catch (error) {
		console.warn("Improper deck found:", error, tbd);
		return false;
	}
}

/**
 * Fetches all deck metadata.
 */
export async function getAllUsers(Astro: AstroGlobal): Promise<Result<Array<UserData>>> {
	// TODO: Paginate
	// Create a prepared statement
	try {
		const decksQuery = Astro.locals.runtime.env.D1.prepare(`
			SELECT *
			FROM users
			ORDER BY display_name ASC
		`);
		const decksResult = await decksQuery.all();
		const decks = decksResult.results.filter(isUserData).map(userDataFromDb);

		return decks;
	} catch (error) {
		console.error(error);
		return errorFromUnknownError(error);
	}
}
