import type { AstroGlobal } from "astro";
import type { DeckData, DeckDbData } from "../structs.js";
import { assert } from "superstruct";
import { deckDbData, deckDataFromDb } from "../structs.js";
import { errorFromUnknownError } from "../errorFromUnknownError.js";

function isDeckData(tbd: unknown): tbd is DeckDbData {
	try {
		assert(tbd, deckDbData);
		return true;
	} catch (error) {
		console.warn("Improper deck found:", error, tbd);
		return false;
	}
}

/**
 * Fetches all deck metadata.
 */
export async function getAllDecks(Astro: AstroGlobal): Promise<Result<Array<DeckData>>> {
	// TODO: Paginate
	// Create a prepared statement
	try {
		const decksQuery = Astro.locals.runtime.env.D1.prepare(`
			SELECT decks.*, COUNT(DISTINCT cards.id) as card_count
			FROM decks
			LEFT JOIN cards ON decks.id=cards.deck_id
			GROUP BY decks.id
			ORDER BY decks.title ASC
		`);
		const decksResult = await decksQuery.all();
		const decks = decksResult.results.filter(isDeckData).map(deckDataFromDb);

		return decks;
	} catch (error) {
		console.error(error);
		return errorFromUnknownError(error);
	}
}
