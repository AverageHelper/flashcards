import type { AstroGlobal } from "astro";
import type { DeckData } from "../structs.js";
import { deckDataFromDb, deckDbData } from "../structs.js";
import { errorFromUnknownError } from "../errorFromUnknownError.js";
import { is, nullable } from "superstruct";

/**
 * Fetches the deck metadata with the given `id` from the database.
 * Returns `null` if no such deck is found, or if the data is malformed.
 */
export async function getDeck(
	Astro: AstroGlobal,
	id: string | undefined,
): Promise<Result<DeckData | null>> {
	// Ignore empty ID
	if (!id) return null;

	try {
		const deckQuery = Astro.locals.runtime.env.D1.prepare(
			`SELECT decks.*, COUNT(DISTINCT cards.id) as card_count
			FROM decks
			LEFT JOIN cards ON decks.id=cards.deck_id
			WHERE decks.id = ?1`,
		).bind(id);
		const deck = await deckQuery.first();
		if (!is(deck, nullable(deckDbData))) return null; // Ignore malformed data
		if (!deck) return null;

		return deckDataFromDb(deck);
	} catch (error) {
		console.error(error);
		return errorFromUnknownError(error);
	}
}
