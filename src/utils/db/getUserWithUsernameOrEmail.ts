import type { AstroGlobal } from "astro";
import type { UserData } from "../structs.js";
import { errorFromUnknownError } from "../errorFromUnknownError.js";
import { is, nullable } from "superstruct";
import { userDataFromDb, userDbData } from "../structs.js";

/**
 * Fetches user metadata from the database where their `username` or `email`
 * matches the given `usernameOrEmail` from the database. Returns `null` if
 * no such user is found, or if the data is malformed.
 */
export async function getUserWithUsernameOrEmail(
	Astro: AstroGlobal,
	usernameOrEmail: string,
): Promise<Result<UserData | null>> {
	// Ignore empty username
	if (!usernameOrEmail) return null;

	try {
		const deckQuery = Astro.locals.runtime.env.D1.prepare(
			`SELECT * FROM users
			WHERE display_name = ?1 OR email = ?1`,
		).bind(usernameOrEmail);
		const deck = await deckQuery.first();
		if (!is(deck, nullable(userDbData))) return null; // Ignore malformed data
		if (!deck) return null;

		return userDataFromDb(deck);
	} catch (error) {
		console.error(error);
		return errorFromUnknownError(error);
	}
}
