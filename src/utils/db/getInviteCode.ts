import type { AstroGlobal } from "astro";
import type { InviteCodeData } from "../structs.js";
import { errorFromUnknownError } from "../errorFromUnknownError.js";
import { inviteCodeDataFromDb, inviteCodeDbData } from "../structs.js";
import { is, nullable } from "superstruct";

/**
 * Fetches the invite code metadata with the given `value` from the database.
 * Returns `null` if no such invite code is found, or if the data is malformed.
 *
 * The comparison is done without regard for character case.
 */
export async function getInviteCode(
	Astro: AstroGlobal,
	value: string,
): Promise<Result<InviteCodeData | null>> {
	// Ignore empty value
	if (!value) return null;

	try {
		const inviteCodeQuery = Astro.locals.runtime.env.D1.prepare(
			`SELECT registration_invites.*, COUNT(DISTINCT users.id) as current_uses
			FROM registration_invites
			LEFT JOIN users ON registration_invites.id=users.invite_code
			WHERE UPPER(registration_invites.value) = ?1`,
		).bind(value.toUpperCase());
		const deck = await inviteCodeQuery.first();
		if (!is(deck, nullable(inviteCodeDbData))) return null; // Ignore malformed data
		if (!deck) return null;

		return inviteCodeDataFromDb(deck);
	} catch (error) {
		console.error(error);
		return errorFromUnknownError(error);
	}
}
