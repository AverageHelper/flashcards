const _disallowedUsernames = new Set<string>();

function addUsername(username: string): void {
	_disallowedUsernames.add(username);
}

// TODO: Combine the lists into one? Am I allowed to do that with MIT licenses?
// TODO: See about using GPL license for this project. Can I do that while incorporating MIT-licensed code?

/* `bad_usernames.*.json` are adapted from https://github.com/flurdy/bad_usernames/tree/ecf2524f2dbe43b774dc8bffb9f229aebd540b43

The MIT License (MIT)
Copyright © 2017 Ivar Abrahamsen

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
import bad_usernames_de from "./usernames/bad_usernames.de.json";
bad_usernames_de.forEach(addUsername);

import bad_usernames_en from "./usernames/bad_usernames.en.json";
bad_usernames_en.forEach(addUsername);

import bad_usernames_no from "./usernames/bad_usernames.no.json";
bad_usernames_no.forEach(addUsername);

/* `disallowed_usernames.json` is adapted from https://github.com/dsignr/disallowed-usernames/blob/311fd6b05600cb40e6940ae381189596fc2d8f3a/disallowed%20usernames.csv,
// with additions from https://github.com/dsignr/disallowed-usernames/issues/11
// and https://github.com/NicoJuicy/Disallowed-usernames/commit/c30cdc7b25f033c2269532cfc8abdf62a3ba452c

The MIT License (MIT)

Copyright (c) 2013 dsignr

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
import disallowed_usernames from "./usernames/disallowed_usernames.json";
disallowed_usernames.forEach(addUsername);

/* `reserved-usernames.json` is adapted from https://github.com/shouldbee/reserved-usernames/blob/0302214e8c1363fc5877ce3343ef6262e8852e96/reserved-usernames.json

The MIT License (MIT)

Copyright (c) 2014

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
import reservedUsernames from "./usernames/reserved-usernames.json";
reservedUsernames.forEach(addUsername);

// Disallow using the platform name
_disallowedUsernames.add("flashcards");

/**
 * A list of words which new users should not use as display names.
 * These are usernames that may confuse users with legitimate platform
 * structures, well-known services, etc., and so are reserved.
 *
 * This list is not exhaustive. We have better ways to deal
 * with abusive usernames.
 */
export const disallowedUsernames: ReadonlySet<string> = _disallowedUsernames;
