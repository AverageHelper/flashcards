import { describe, expect, test, vi } from "vitest";
import { is } from "superstruct";
import { randomUUID, uuid } from "./uuid.js";

describe("UUID", () => {
	test("generates a unique identifier", () => {
		expect(randomUUID()).toStrictEqual(expect.stringContaining(""));
	});

	test("regenerates a unique identifier if we discover an edge case", () => {
		const mockRandomUUID = vi
			.spyOn(crypto, "randomUUID")
			.mockReturnValueOnce("testing-the-very-worst-case-scenario");

		expect(randomUUID()).toStrictEqual(expect.stringContaining(""));
		expect(mockRandomUUID).toHaveBeenCalledTimes(2);

		mockRandomUUID.mockRestore();
	});

	test.each([
		"36b8f84d-df4e-4d49-b662-bcde71a8764f", // from MDN
		"00000000-0000-0000-0000-000000000000", // the NIL UUID
		"a04d8725-ec15-4f11-9112-167b71b8d30a", // our admin user
		"ade447c2-6677-4d70-8b86-83ecf897760a",
		"280a3804-40ac-46cb-94a4-52ee6299de9b",
	] as const)("struct matches valid UUID string '%s'", sample => {
		expect(is(sample, uuid())).toBe(true);
	});
});
