import type { Struct } from "superstruct";
import { assert, is, pattern, size, string } from "superstruct";

export type UUID = ReturnType<(typeof crypto)["randomUUID"]>;

/**
 * Generates a new cryptographically secure unique identifier.
 */
export function randomUUID(): UUID {
	const result = crypto.randomUUID();

	// Sanity check: Validate the result against the `uuid()` struct
	if (is(result, uuid())) return result;

	// Try again if we failed
	const newResult = crypto.randomUUID();
	assert(newResult, uuid());
	return newResult;
}

/** See https://stackoverflow.com/a/13653180 */
const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/iu;

/**
 * Ensure that a string is a valid v4 UUID per RFC4122, as generated
 * by `crypto.randomUUID()`.
 */
export function uuid(): Struct<UUID, null> {
	return pattern(size(string(), 36), uuidRegex) as unknown as Struct<UUID, null>;
}
