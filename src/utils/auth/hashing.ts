import type { AstroGlobal } from "astro";
import type { Opaque } from "type-fest";
import { env } from "../environment.js";
import bcrypt from "bcryptjs";

const { genSalt, hash: _hash, compare } = bcrypt;

export const PASSWORD_MAX_BYTES = 72; // bcrypt limitation

export type PasswordPotatoes = Opaque<string, "hashed-password">;

type Salt = Opaque<string, "salt">;

/**
 * Resolves with a password hash.
 */
export async function hash(Astro: AstroGlobal, password: string): Promise<PasswordPotatoes> {
	const salt = (await genSalt(11)) as Salt;
	return await hashWithSalt(Astro, password, salt);
}

async function hashWithSalt(
	Astro: AstroGlobal,
	password: string,
	salt: Salt,
): Promise<PasswordPotatoes> {
	// 0. Check that auth is enabled
	if (!env(Astro).isLoginEnabled) throw new TypeError("Login is disabled");

	// 1. Normalize the password so users don't get confused over Unicode equivalence
	const pwd = normalized(password);

	// 2. Limit password to 72 bytes, per OWASP recommendations for bcrypt
	if (new Blob([pwd]).size > PASSWORD_MAX_BYTES) throw new TypeError("String too large");

	// 3. Derive key
	const result = await _hash(pwd, salt);
	return result as PasswordPotatoes;
}

/**
 * Resolves with `true` if the given `password` hashes into the same data as `potatoes`.
 */
export async function verify(
	Astro: AstroGlobal,
	potatoes: PasswordPotatoes,
	password: string,
): Promise<boolean> {
	// Don't pass if login is disabled
	if (!env(Astro).isLoginEnabled) return false;

	// Remember to normalize input
	const pwd = normalized(password);

	return await compare(pwd, potatoes);
}

function normalized(str: string): string {
	return str.normalize("NFKD");
}
