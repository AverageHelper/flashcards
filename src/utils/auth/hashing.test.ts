import type { AstroGlobal } from "astro";
import type { Env, env } from "../environment.js";
import type { PasswordPotatoes } from "./hashing.js";
import { beforeEach, describe, expect, test, vi } from "vitest";
import { hash, verify } from "./hashing.js";

const mockEnv = vi.hoisted(() => vi.fn<Parameters<typeof env>, ReturnType<typeof env>>());
vi.mock("./environment.js", () => ({ env: mockEnv }));

describe("Password hashing", () => {
	const Astro = undefined as unknown as AstroGlobal;

	beforeEach(() => {
		mockEnv.mockReturnValue({
			D1: undefined as unknown as D1Database,
			isLoginEnabled: true,
			userRegistrationMode: "disabled",
		});
	});

	test("hash fails if auth is disabled", async () => {
		mockEnv.mockReturnValueOnce({ isLoginEnabled: false } as unknown as Env);
		await expect(() => hash(Astro, "foo")).rejects.toThrow(TypeError);
	});

	test("hash differs from password", async () => {
		const pwd = "foo";
		const result = await hash(Astro, pwd);
		expect(result).toStrictEqual(expect.stringContaining(""));
		expect(result).not.toBe(pwd);
	});

	test("hash differs between invocations (new salt)", async () => {
		const pwd = "foo";
		const hash1 = await hash(Astro, pwd);
		const hash2 = await hash(Astro, pwd);
		expect(hash1).not.toBe(hash2);
	});

	test("hash is stable for same user and password", async () => {
		const pwd = "foo";
		const material = await hash(Astro, pwd);
		await expect(verify(Astro, material, pwd)).resolves.toBe(true);
	});

	test("incorrect password does not match hash", async () => {
		const material = await hash(Astro, "foo");
		await expect(verify(Astro, material, "bar")).resolves.toBe(false);
	});

	test("password fails to verify if auth is disabled", async () => {
		const pwd = "foo";
		const material = await hash(Astro, pwd);
		mockEnv.mockReturnValueOnce({ isLoginEnabled: false } as unknown as Env);
		await expect(verify(Astro, material, pwd)).resolves.toBe(false);
	});

	test.each([
		"inert" as PasswordPotatoes, // the special value for the admin user, should preclude auth
		"" as PasswordPotatoes,
		"0" as PasswordPotatoes,
	] as const)("hash fails to verify if hash is invalid ('%s')", async material => {
		const pwd = "foo";
		await expect(verify(Astro, material, pwd)).resolves.toBe(false);
	});
});
