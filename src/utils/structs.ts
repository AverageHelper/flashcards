import type { Infer, Struct } from "superstruct";
import type { PasswordPotatoes } from "./auth/index.js";
import type { UUID } from "./uuid.js";
import {
	enums,
	integer,
	min,
	nonempty,
	nullable,
	optional,
	pattern,
	size,
	string,
	trimmed,
	type,
} from "superstruct";
import { lowercased } from "./lowercased.js";
import { PASSWORD_MAX_BYTES } from "./auth/index.js";
import { Temporal } from "temporal-polyfill";
import { uuid } from "./uuid.js";

//#region Form Validation

/**
 * The ID of our system user. Normal users should not be permitted to
 * do actions on behalf of this user.
 */
export const ADMIN_USER_ID = "a04d8725-ec15-4f11-9112-167b71b8d30a";

// Password reqs.
export const PASSWORD_MAX_LENGTH = PASSWORD_MAX_BYTES;
export const PASSWORD_MIN_LENGTH = 12;

// Using a string here because <input> doesn't like the result of `RegExp.prototype.toString`
export const passwordPattern = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\\W).{12,}$";

export function password(): Struct<string, null> {
	return pattern(
		// Do NOT trim the password.
		size(nonemptyString(), PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH),
		new RegExp(passwordPattern, "u"),
	);
}

// Username reqs.
export const USERNAME_MIN_LENGTH = 3;
export const USERNAME_MAX_LENGTH = 30;
export { disallowedUsernames } from "@/utils/disallowedUsernames.js";

/**
 * - Lowercase alphanumeric characters and `_`
 * - Must begin with a letter
 * - Must not end with `_`
 */
export const usernamePattern = "^[a-z][a-z_0-9]+[a-z0-9]$";

export function username(): Struct<string, null> {
	return pattern(
		lowercased(size(nonemptyTrimmedString(), USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)),
		new RegExp(usernamePattern, "u"),
	);
}

// Email reqs.

export function email(): Struct<string, null> {
	// Must contain an @ symbol somewhere
	return pattern(nonemptyTrimmedString(), /@/u);
}

// Invite code

export function invitationCode(): Struct<string, null> {
	// May be any string, as long as it's not empty
	return nonemptyTrimmedString();
}

//#endregion Form Validation

//#region Invite Codes

export const inviteCodeDbData = type({
	/** The invite code's unique identifier. */
	id: uuid(),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the invite code was created. */
	created_at: integer(),

	/** The unique ID of the user that created the invite code. */
	created_by: uuid(),

	/** The string users must enter to actuate this code. */
	value: nonemptyString(),

	/** The total number of times this code may be used. */
	total_uses: min(integer(), 0),

	/** The number of individual users that have used this invite code, derived from a JOIN on the `users` table. */
	current_uses: optional(integer()),
});

export type InviteCodeDbData = Infer<typeof inviteCodeDbData>;

export interface InviteCodeData
	extends Omit<InviteCodeDbData, "created_at" | "created_by" | "total_uses" | "current_uses"> {
	/** The instant at which the invite code was created. */
	createdAt: Temporal.Instant;

	/** The unique ID of the user that created the invite code. */
	createdBy: UUID;

	/** The total number of times this code may be used. */
	totalUses: number;

	/** The number of individual users that have used this invite code. */
	currentUses: number;
}

/** Constructs invite code data from the given database representation. */
export function inviteCodeDataFromDb(data: InviteCodeDbData): InviteCodeData {
	return {
		id: data.id,
		createdAt: Temporal.Instant.fromEpochMilliseconds(data.created_at),
		createdBy: data.created_by,
		value: data.value,
		totalUses: data.total_uses,
		currentUses: data.current_uses ?? data.total_uses, // Assume the code is expended if we failed to fetch the user data
	};
}

/** Constructs a database representation of the given invite code data. */
export function inviteCodeDataToDb(data: InviteCodeData): Omit<InviteCodeDbData, "current_uses"> {
	return {
		id: data.id,
		created_at: data.createdAt.epochMilliseconds,
		created_by: data.createdBy,
		value: data.value,
		total_uses: data.totalUses,
		// Intentionally omitting derived properties like `current_uses`
	};
}

//#endregion Invite Codes

//#region Users

export const userDbData = type({
	/** The user's unique identifier. */
	id: uuid(),

	/** The unique identifier of the invite code the user used to register, or `null` if they did not use an invite code. */
	invite_code: nullable(uuid()),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the user was created. */
	created_at: integer(),

	/** The name with which the user appears to other users. */
	display_name: nonemptyString(),

	/** The user's email address. */
	email: nonemptyString(),

	// TODO: Previous email. Delete this data once the current email is verified. We keep this value so users can recover their account if it's stolen and the address is changed. We should email the old address if ever the email changes, and provide a unique link that the user may click to revert the change.

	/** Whether the user's email address has been verified. `0` for `false`, `1` for `true`. */
	is_email_verified: enums([0, 1]),

	/** The hash, salt, and other metadata of the user's password. */
	password_potatoes: nonemptyString() as unknown as Struct<PasswordPotatoes, null>,

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the user last updated their password. */
	password_updated_at: integer(),

	/** Whether the user should be permitted to have any active auth sessions. `0` for `false`, `1` for `true`. */
	allows_login: enums([0, 1]),

	/** The reason a user's login access was revoked. */
	ban_reason: nullable(nonemptyString()),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when we last sent the user a verification email, or `null` if no such email was ever sent. */
	email_verification_last_sent: nullable(integer()),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when we last sent the user a password reset email, or `null` if no such email was ever sent. */
	password_reset_email_last_sent: nullable(integer()),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the user last accepted our Terms of Service and Privacy Policy, or `null` if they never did. */
	tos_last_accepted: nullable(integer()),
});

export type UserDbData = Infer<typeof userDbData>;

export interface UserData
	extends Omit<
		UserDbData,
		| "invite_code"
		| "created_at"
		| "display_name"
		| "is_email_verified"
		| "password_potatoes"
		| "password_updated_at"
		| "allows_login"
		| "ban_reason"
		| "email_verification_last_sent"
		| "password_reset_email_last_sent"
		| "tos_last_accepted"
	> {
	/** The unique identifier of the invite code the user used to register, or `null` if they did not use an invite code. */
	inviteCode: UUID | null;

	/** The instant at which the user was created. */
	createdAt: Temporal.Instant;

	/** The name with which the user appears to other users. */
	displayName: string;

	/** Whether the user's email address has been verified. */
	isEmailVerified: boolean;

	/** The hash, salt, and other metadata of the user's password. */
	passwordPotatoes: PasswordPotatoes;

	/** The instant at which the user last updated their password. */
	passwordUpdatedAt: Temporal.Instant;

	/** Whether the user should be permitted to have any active auth sessions. */
	allowsLogin: boolean;

	/** The reason a user's login access was revoked. */
	banReason: string | null;

	/** The instant at which we last sent the user a verification email, or `null` if none was ever sent. */
	emailVerificationLastSent: Temporal.Instant | null;

	/** The instant at which we last sent the user a password reset email, or `null` if none was ever sent. */
	passwordResetEmailLastSent: Temporal.Instant | null;

	/** The instant at which the user last accepted our Terms of Service and Privacy Policy, or `null` if they never did. */
	tosLastAccepted: Temporal.Instant | null;
}

/** Constructs user data from the given database representation. */
export function userDataFromDb(data: UserDbData): UserData {
	return {
		id: data.id,
		inviteCode: data.invite_code,
		createdAt: Temporal.Instant.fromEpochMilliseconds(data.created_at),
		displayName: data.display_name.trim(),
		email: data.email.trim(),
		isEmailVerified: data.is_email_verified === 1,
		passwordPotatoes: data.password_potatoes,
		passwordUpdatedAt: Temporal.Instant.fromEpochMilliseconds(data.password_updated_at),
		allowsLogin: data.allows_login === 1,
		banReason: data.ban_reason?.trim() || null,
		emailVerificationLastSent:
			data.email_verification_last_sent === null
				? null
				: Temporal.Instant.fromEpochMilliseconds(data.email_verification_last_sent),
		passwordResetEmailLastSent:
			data.password_reset_email_last_sent === null
				? null
				: Temporal.Instant.fromEpochMilliseconds(data.password_reset_email_last_sent),
		tosLastAccepted:
			data.tos_last_accepted === null
				? null
				: Temporal.Instant.fromEpochMilliseconds(data.tos_last_accepted),
	};
}

/** Constructs a database representation of the given user data. */
export function userDataToDb(data: UserData): UserDbData {
	return {
		id: data.id,
		invite_code: data.inviteCode,
		created_at: data.createdAt.epochMilliseconds,
		display_name: data.displayName.trim(),
		email: data.email.trim(),
		is_email_verified: data.isEmailVerified ? 1 : 0,
		password_potatoes: data.passwordPotatoes,
		password_updated_at: data.passwordUpdatedAt.epochMilliseconds,
		allows_login: data.allowsLogin ? 1 : 0,
		ban_reason: data.banReason?.trim() || null,
		email_verification_last_sent: data.emailVerificationLastSent?.epochMilliseconds ?? null,
		password_reset_email_last_sent: data.passwordResetEmailLastSent?.epochMilliseconds ?? null,
		tos_last_accepted: data.tosLastAccepted?.epochMilliseconds ?? null,
	};
}

//#endregion Users

//#region Decks

export const deckDbData = type({
	/** The deck's unique identifier. */
	id: uuid(),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the deck was created. */
	created_at: integer(),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the deck was last modified. */
	last_modified_at: integer(),

	/** The unique ID of the user that created the deck. */
	created_by: uuid(),

	/** The name of the deck. */
	title: nonemptyString(),

	/** Text that describes the deck. */
	description: nonemptyString(),

	/** The number of cards in the deck, derived from a JOIN on the `cards` table. */
	card_count: optional(integer()),
});

export type DeckDbData = Infer<typeof deckDbData>;

export interface DeckData
	extends Omit<DeckDbData, "created_at" | "last_modified_at" | "created_by" | "card_count"> {
	/** The instant at which the deck was created. */
	createdAt: Temporal.Instant;

	/** The instant at which the deck was last modified. */
	lastModifiedAt: Temporal.Instant;

	/** The unique ID of the user that created the deck. */
	createdBy: UUID;

	/** The number of cards in the deck. */
	cardCount: number;
}

/** Constructs deck data from the given database representation. */
export function deckDataFromDb(data: DeckDbData): DeckData {
	return {
		id: data.id,
		createdAt: Temporal.Instant.fromEpochMilliseconds(data.created_at),
		lastModifiedAt: Temporal.Instant.fromEpochMilliseconds(data.last_modified_at),
		createdBy: data.created_by,
		title: data.title.trim(),
		description: data.description.trim(),
		cardCount: data.card_count ?? 0,
	};
}

/** Constructs a database representation of the given deck data. */
export function deckDataToDb(data: DeckData): Omit<DeckDbData, "card_count"> {
	return {
		id: data.id,
		created_at: data.createdAt.epochMilliseconds,
		last_modified_at: data.lastModifiedAt.epochMilliseconds,
		created_by: data.createdBy,
		title: data.title.trim(),
		description: data.description.trim(),
		// Intentionally omitting derived properties like `card_count`
	};
}

//#endregion Decks

//#region Cards

export const cardDbData = type({
	/** The card's unique identifier. */
	id: uuid(),

	/** The unique identifier of the deck to which the card belongs. */
	deck_id: uuid(),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the card was created. */
	created_at: integer(),

	/** The number of seconds since 1970-01-01 00:00:00 UTC when the card was last modified. */
	last_modified_at: integer(),

	/** The unique ID of the user that created the card. */
	created_by: uuid(),

	/** The text at the front of the card. */
	front: nonemptyString(),

	/** The text at the back of the card. */
	back: nonemptyString(),
});

export type CardDbData = Infer<typeof cardDbData>;

export interface CardData
	extends Omit<CardDbData, "deck_id" | "created_at" | "last_modified_at" | "created_by"> {
	/** The unique identifier of the deck to which the card belongs. */
	deckId: UUID;

	/** The instant at which the card was created. */
	createdAt: Temporal.Instant;

	/** The instant at which the card was last modified. */
	lastModifiedAt: Temporal.Instant;

	/** The unique ID of the user that created the card. */
	createdBy: UUID;
}

/** Constructs card data from the given database representation. */
export function cardDataFromDb(data: CardDbData): CardData {
	return {
		id: data.id,
		deckId: data.deck_id,
		createdAt: Temporal.Instant.fromEpochMilliseconds(data.created_at),
		lastModifiedAt: Temporal.Instant.fromEpochMilliseconds(data.last_modified_at),
		createdBy: data.created_by,
		front: data.front.trim(),
		back: data.back.trim(),
	};
}

/** Constructs a database representation of the given card data. */
export function cardDataToDb(data: CardData): CardDbData {
	return {
		id: data.id,
		deck_id: data.deckId,
		created_at: data.createdAt.epochMilliseconds,
		last_modified_at: data.lastModifiedAt.epochMilliseconds,
		created_by: data.createdBy,
		front: data.front.trim(),
		back: data.back.trim(),
	};
}

//#endregion Cards

function nonemptyString(): Struct<string, null> {
	return nonempty(string());
}

function nonemptyTrimmedString(): Struct<string, null> {
	return nonempty(trimmed(string()));
}

export function isNotNullOrFalse<T>(tbd: T | null | false): tbd is T {
	return tbd !== null && tbd !== false;
}

/**
 * The expected layout of data on the database.
 */
export const databaseTables = {
	users: userDbData,
	decks: deckDbData,
	cards: cardDbData,
} as const;
