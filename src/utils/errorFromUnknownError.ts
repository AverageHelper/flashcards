/**
 * Creates and returns an `Error` instance from the given value.
 * If the given object is already an instance of `Error`, then
 * the value is returned as-is.
 */
export function errorFromUnknownError(error: unknown): Error {
	if (error instanceof Error) {
		return error;
	} else if (typeof error === "string") {
		return new Error(error);
	}
	return new Error(JSON.stringify(error));
}
