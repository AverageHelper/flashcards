import type { Struct } from "superstruct";
import { coerce, string } from "superstruct";

/**
 * Augment a struct to lowercase string inputs.
 *
 * Note: You must use `create(value, Struct)` on the value to have the
 * coercion take effect! Using simply `assert()` or `is()` will not use coercion.
 */
export function lowercased<T, S>(struct: Struct<T, S>): Struct<T, S> {
	return coerce(struct, string(), x => x.toLowerCase());
}
