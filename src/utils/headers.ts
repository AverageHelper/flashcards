import type { AstroGlobal } from "astro";

/**
 * Sets headers from `public/_headers`.
 *
 * @param Astro The `Astro` context object.
 */
export function setStandardHeaders(Astro: Pick<AstroGlobal, "url" | "response">): void {
	const { url, response: res } = Astro;

	function setHeader(header: string, value: string): void {
		res.headers.set(header, value);
	}

	// The _headers file does not apply to function endpoints, so we have to do that ourselves:
	if (url.hostname.endsWith("pages.dev")) {
		// Don't show pages.dev deployment in search results
		setHeader("X-Robots-Tag", "noindex");
	} else {
		// CORS
		setHeader("Access-Control-Allow-Origin", "https://flashcards.average.name");

		// Disable caching
		setHeader("Vary", "*");

		// Browsers should avoid loading from unknown domains
		setHeader(
			"Content-Security-Policy",
			"default-src 'none'; img-src 'self' data:; media-src 'self' data:; style-src 'self' 'unsafe-inline'; frame-ancestors 'self'; upgrade-insecure-requests",
		);

		// Browsers should disable features we do not use
		setHeader(
			"Permissions-Policy",
			"accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), clipboard-read=(), clipboard-write=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(), gamepad=(), geolocation=(), gyroscope=(), identity-credentials-get=(), idle-detection=(), interest-cohort=(), keyboard-map=(), local-fonts=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-create=(), publickey-credentials-get=(), screen-wake-lock=(), serial=(), speaker-selection=(), storage-access=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=()",
		);

		// Browsers should prefer HTTPS
		setHeader("Strict-Transport-Security", "max-age=2592000; includeSubDomains; preload");

		// Browsers should be cautious about loading resources from other origins
		setHeader("Cross-Origin-Embedder-Policy", "require-corp");

		// Browsers should not load cross-origin documents
		setHeader("Cross-Origin-Opener-Policy", "same-origin");

		// Browsers should not load this resource from another site
		setHeader("Cross-Origin-Resource-Policy", "same-site");

		// Browsers shouldn't expect executable MIME types from requests for non-executable files
		setHeader("X-Content-Type-Options", "nosniff");

		// Browsers shouldn't load us in an iframe unless the page is ours
		setHeader("X-Frame-Options", "SAMEORIGIN");

		// Browsers shouldn't send the `Referer` header with sent requests
		setHeader("Referrer-Policy", "no-referrer");

		// Miscellaneous
		setHeader("X-Clacks-Overhead", "GNU Nex Benedict");
	}

	if (url.host === "localhost:4321" || url.host === "localhost:8788") {
		// If we're in Astro's dev mode, we should permit local scripts
		setHeader(
			"Content-Security-Policy",
			"default-src 'self'; img-src 'self' data:; media-src 'self' data:; style-src 'self' 'unsafe-inline'; child-src 'none'; object-src 'none'; worker-src 'none'; frame-ancestors 'self'; upgrade-insecure-requests",
		);
	}
}
