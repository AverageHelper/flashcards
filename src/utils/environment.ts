import type { AstroGlobal } from "astro";
import type { Infer } from "superstruct";
import { enums, is } from "superstruct";

const registrationMode = enums(["disabled", "invite-only", "open"]);
type RegistrationMode = Infer<typeof registrationMode>;

export interface Env extends Pick<ENV, "D1"> {
	/**
	 * The manner in which new users are permitted to register an account.
	 *
	 * - `"disabled"` - The default registration mode. Prohibits new users from joining.
	 * - `"invite-only"` - Users are required to provide a unique invitation code to join.
	 * - `"open"` - Any user may create a new account.
	 */
	readonly userRegistrationMode: RegistrationMode;

	/** Whether users may authenticate. */
	readonly isLoginEnabled: boolean;
}

/**
 * Returns a clean environment object from Astro.
 */
export function env(Astro: AstroGlobal): Env {
	const e = Astro.locals.runtime.env;
	return {
		...e,
		userRegistrationMode: is(e.USER_REGISTRATION_MODE, registrationMode)
			? e.USER_REGISTRATION_MODE
			: "disabled",
		isLoginEnabled: e.IS_AUTH_ENABLED === "true",
	};
}
